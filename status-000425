Pango Status Report #6     25 Apr 2000
======================================

General news:

 Robert Brady has contributed a shaper for Devanagari.
 Eventually, we hopefully we can share some code between
 the different Indic languages, but this looks like
 a very good start. 

 I've added a couple of pages to www.pango.org with information
 about topics related to GTK+. One page is about
 input methods and keyboard maps:
 
  http://www.pango.org/input-resources.shtml

 The other about fonts:

  http://www.pango.org/font-resources.shtml

 These are still pretty incomplete, but I hope they will 
 eventually be useful resources. Let me know if there is
 information that you think should be added to them.

 Uri David Akavia created a XKB map for Hebrew which
 is linked to from the input-resources page. 


Recent progress:

 The Pango-ization of the GTK+ port of the Tk Text widget
 is pretty much complete now. Its almost as functional
 and stable as the pre-Pango version, and the bidirectional
 editing behavior should be at least approximately right.

 I've made just a few additions to Pango to support the
 work I've been doing in GTK+. I've added support functions
 for getting selection ranges, visual cursor navigation,
 and similar tasks.

 Quite a few bugs in Pango and the Pango branch of GTK+
 have been fixed.


Releases:

 Pango-0.10 is on available from www.pango.org. 
 RPM's of pango, libunicode and libfribidi are also there.
 
 I've also put up snapshots of GTK+ with Pango support
 at the same place. These are very much unstable and
 unsupported; but if any brave souls want to try them
 out and give suggestions (or fixes) for the editing
 behavior, I'd appreciate it.

 This time around, there are also RPMS of the GTK+ snapshots 
 and their dependencies. These are supposed to be pretty easy 
 to install. I spent some time today making the testtext 
 program useable as a miniature text text editor. So,
 if you try out the RPMS, this may be fun to play with.


TODO highlights:

  The projects I intend to tackle next are:

    - Write some documentation about writing shapers.

    - Remove GDK fonts from the last few GTK+ widgets (things like GtkRuler)

    - Figure what the font interfaces will look like for GTK+-1.4

    - Start doing some performance analysis of Pango. (The Text widget
      is noticeably slow with a couple of thousand lines of text.)


  Some interesting projects that other people might want to consider:

   - Write a libart based font-system and renderer to go along with
     the X based one. It would be good to have an idea about how
     well the interfaces work with something other than X before
     we get too far along. (Alternatively, write a FreeType-based
     font-system and renderer to go along with the X-based one.)

   - Write a shaping engine for whatever language you are interested in.

   - Come up with a XKB-based keyboard map for Arabic, so people
     can test out the GTK+ editing support with Arabic. (If you
     are interested this but don't know how to procede with this,
     let me know and I can give some hints.)

   - Look at writing an XIM module for the new input-method framework
     in GTK+. (It may still be a bit early, but if you would
     be interested in working on this, let me know. We can discuss 
     what is involved.)


Misc stuff:

 May 1 is the deadline for paper submisssions for both the
 Unicode conference:

  http://www.unicode.org/iuc/iuc17/call.txt

 and the Atlanta Linux Symposium:

  http://www.linuxshowcase.org/cfp/


Owen Taylor
