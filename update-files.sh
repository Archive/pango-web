#!/bin/sh

base="http://developer.gnome.org/doc/API/2.0/pango/"

wget $base/index.html -O tmp.html || exit 1

( tidy tmp.html 2>/dev/null | \
    perl -ne 's@href=\"@href=\"http://developer.gnome.org/doc/API/2.0/pango/@; s/&mdash;/&#8212;/; print if /<div class="TOC">/../<\/div>/' > contents.html ) || exit 1

rm -f tmp.html

#cd pango/examples
#code2html c viewer.c > ../../viewer.html
#cd ..
#cp TODO ..

