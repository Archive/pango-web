#!/usr/bin/perl 

if (@ARGV < 0 || @ARGV[0] !~ /status-(\d\d)(\d\d)(\d\d)/) {
    die "Usage: $0 status-YYMMDD\n";
}

@months = qw(January February March April May June July August September October November December);

$year = "20$1";
$month = @months[$2 - 1];
$day = 0 + $3;

open INFILE, "<$ARGV[0]" or die "Cannot open $ARGV[0]: $!";
open OUTFILE, ">$ARGV[0].shtml" or die "Cannot open $ARGV[0].shtml: $!";

print OUTFILE <<EOT;
<!--#set var="section" value="Status - $day $month $year" -->
<!--#include file="head.shtml" -->
  <td class="main">
  <pre>
EOT

sub relurl {
    $str = shift;
    $str =~ s@http://www.pango.org/@@;
    $str;
}

while (<INFILE>) {
    s@((?:http|ftp)://\S*)@qq(<a href=").relurl($1).qq(">$1</a>)@e;
    print OUTFILE $_;
}

print OUTFILE <<EOT;
  </pre>
  </td>
<!--#include file="foot.shtml" -->
EOT

close INFILE;
close OUTFILE;
